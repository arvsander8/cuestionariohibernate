package com.modelo;

// Generated 14/07/2014 08:12:33 AM by Hibernate Tools 3.4.0.CR1
import com.modelo.Alternative;
import java.util.List;
import java.util.ArrayList;
/**
 * Question generated by hbm2java
 */
public class Question implements java.io.Serializable {

	private int id;
	private Integer code;
	private String question;
	private String comments;
	private Integer state;
	private Integer iddifficulty;
	private Integer alternativecorrect;
	private Integer idquestionnaire;
	//agregamos un campo m�s.. que es el arraylist.
	private List<Alternative> alternativas;

	public Question() {
	}

	public Question(int id) {
		this.id = id;
	}

	public Question(int id, Integer code, String question, String comments,
			Integer state, Integer iddifficulty, Integer alternativecorrect,
			Integer idquestionnaire) {
		this.id = id;
		this.code = code;
		this.question = question;
		this.comments = comments;
		this.state = state;
		this.iddifficulty = iddifficulty;
		this.alternativecorrect = alternativecorrect;
		this.idquestionnaire = idquestionnaire;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getCode() {
		return this.code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getQuestion() {
		return this.question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getIddifficulty() {
		return this.iddifficulty;
	}

	public void setIddifficulty(Integer iddifficulty) {
		this.iddifficulty = iddifficulty;
	}

	public Integer getAlternativecorrect() {
		return this.alternativecorrect;
	}

	public void setAlternativecorrect(Integer alternativecorrect) {
		this.alternativecorrect = alternativecorrect;
	}

	public Integer getIdquestionnaire() {
		return this.idquestionnaire;
	}

	public void setIdquestionnaire(Integer idquestionnaire) {
		this.idquestionnaire = idquestionnaire;
	}
	
	public List<Alternative> getAlternativas() {
		return this.alternativas;
	}

	public void setAlternativas(List<Alternative> alternativas) {
		this.alternativas = alternativas;
	}

}

