package com.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.primefaces.component.log.Log;

import com.funciones.HibernateUtil;
import com.modelo.Alternative;
import com.modelo.Question;

import java.util.List;
import java.util.ArrayList;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class AlternativaDAO {
	
	private Session session;
	
	public List<Alternative> Alternativas(Question qt) throws Exception{
		
		List<Alternative> lal = new ArrayList<Alternative>();
		Alternative al = null;
		Integer n = 0;
		
		try{
			
			session = HibernateUtil.getSessionfactory().openSession();
			String hql = "FROM Alternative WHERE idquestion = " + qt.getId() + "";
			
			Query query = session.createQuery(hql);
			
			if(!query.list().isEmpty()){
				
				n=query.list().size();
				
				for(Integer i=0; i<= n-1; i++)
				{
					al = (Alternative) query.list().get(i);
					lal.add(al);
			        
			             
				}
				
			}
			
		}catch(Exception e){
			
			throw e;
			
		}
		return lal;
	}

}
