package com.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.primefaces.component.log.Log;

import com.funciones.HibernateUtil;
import com.modelo.Question;
import com.modelo.Questionnaire;

import java.util.List;
import java.util.ArrayList;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;


public class PreguntaDAO {

	private Session session;
	
	public List<Question> Preguntas(Questionnaire qu) throws Exception{
			
			List<Question> lqt = new ArrayList<Question>();
			Question qt = null;
			Integer n = 0;
			
			try{
				
				session = HibernateUtil.getSessionfactory().openSession();
				String hql = "FROM Question WHERE idquestionnaire = " + qu.getId() + "";
				
				Query query = session.createQuery(hql);
				
				if(!query.list().isEmpty()){
					
					n=query.list().size();
					
					for(Integer i=0; i<= n-1; i++)
					{
						qt = (Question) query.list().get(i);
						lqt.add(qt);
				        
				             
					}
					
				}
				
			}catch(Exception e){
				
				throw e;
				
			}
			return lqt;
		}
	
}
