package com.hibernate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="questionnaire")
public class Cuestionario  implements Serializable {

    @Id
    @Column(name="id")
    private int id;
    
    @Column(name="idtopic")
    private int idtopic;
    
    @Column(name="title")
    private String title;
    
    @Column(name="type")    
    private int type;
         
    public Cuestionario(){ 
    }

    public Cuestionario(int id, int idtopic, String title, int type) {
        this.id = id;
        this.idtopic = idtopic;
        this.title = title;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdtopic() {
        return idtopic;
    }

    public void setIdtopic(int idtopic) {
        this.idtopic = idtopic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
	
}
