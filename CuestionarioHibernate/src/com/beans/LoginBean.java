package com.beans;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.modelo.Users;
import com.dao.*;
@ManagedBean
@RequestScoped
public class LoginBean{
	private Users usuario  = new Users();

	public Users getUsuario() {
		return usuario;
	}

	public void setUsuario(Users usuario) {
		this.usuario = usuario;
	} 
	
	public String verificarDatos() throws Exception{
		
		UsuarioDAO usuDAO = new UsuarioDAO();
		Users us;
		String resultado;
		
		try{
			us = usuDAO.verifiacarDatos(this.usuario);
			if(us!=null){
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", us);
				resultado = "cuestionario";
			}
			else{
				resultado = "error";
			}
			
		}catch(Exception e){
			throw e;
		}
		
		
		return resultado;
		
	}
	
	public boolean verificarSesion(){
		boolean estado;
		if(FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario")==null){
			estado = false;
		}
		else{
			estado = true;
		}
		return estado;
	}
	
	public String cerrarSesion(){
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "index";
	}
}
