package com.beans;

import com.objetos.Cuestionary;



//import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.ManagedProperty;
//import javax.faces.bean.ViewScoped;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.dao.PreguntaDAO;
import com.dao.AlternativaDAO;
import com.modelo.Alternative;
import com.modelo.Question;
import com.modelo.Questionnaire;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class CuestionarioBean {
		
	private String user = "0"; // nombre delusuario
	
	private String elegido = ""; //cuestionario elegido
	
	private PreguntaDAO preDAO = new PreguntaDAO();
	private AlternativaDAO altDAO = new AlternativaDAO();
	
	private Questionnaire cu = new Questionnaire();//para crear el cuestionario
	private Question qt = new Question();//para crear una pregunta
	
	private Integer Idd = 0;
	private Integer Ida = 0;

	private List<Question> preguntas = new ArrayList<Question>(); //definimos la lista de cuestionarios
	
	public void setUser(String iuser){

		this.user=iuser;
	}
	
	public String getUser(){
		
		return user;
	}
	
	public String getElegido() {
		return elegido;
	}

	public void setElegido(String elegido) {
		this.elegido = elegido;
	}

	public void Cargarpreguntas(){

		BasicConfigurator.configure();
        Logger log = Logger.getLogger("Logger de Cuestionario DAO");
        log.debug("Cargando Preguntas");
		
		try {
			Idd=Integer.parseInt(this.elegido);
			cu.setId(Idd);
			
			preguntas = preDAO.Preguntas(cu);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public List<Alternative> Cargaralternativas(String ids){

		List<Alternative> lal = new ArrayList<Alternative>();
		
		try {
			Ida = Integer.parseInt(ids);
			qt.setId(Ida);
			
			lal = altDAO.Alternativas(qt);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lal;
		
	}
	
	public void Cargarcuestionario(){
		
		//cargamos las preguntas
		Cargarpreguntas();
		
		//cargamos a cada pregunta sus alternativas
		Integer n = 0;
		List<Alternative> arrayalt;
		Integer idq = 0; 
		
		n = preguntas.size();
		for(Integer i=0;i < n ; i++){
			idq = preguntas.get(i).getId();
			arrayalt = Cargaralternativas(idq.toString());
			preguntas.get(i).setAlternativas(arrayalt);
			
		}
		
		
	}

	public List<Question> getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(List<Question> preguntas) {
		this.preguntas = preguntas;
	}
	
}
